# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160309181417) do

  create_table "categories", force: :cascade do |t|
    t.string   "category",   limit: 255, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "categories", ["category"], name: "index_categories_on_category", unique: true, using: :btree

  create_table "items", force: :cascade do |t|
    t.string   "name",           limit: 255,               null: false
    t.string   "Artist",         limit: 255,               null: false
    t.string   "Album",          limit: 255,               null: false
    t.integer  "year",           limit: 4,                 null: false
    t.text     "link",           limit: 65535,             null: false
    t.integer  "Noofdown",       limit: 4,     default: 0, null: false
    t.integer  "NoofViews",      limit: 4,     default: 0, null: false
    t.text     "Details",        limit: 65535,             null: false
    t.text     "img",            limit: 65535,             null: false
    t.integer  "category_id",    limit: 4
    t.integer  "subcategory_id", limit: 4
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  add_index "items", ["category_id"], name: "index_items_on_category_id", using: :btree
  add_index "items", ["subcategory_id"], name: "index_items_on_subcategory_id", using: :btree

  create_table "subcategories", force: :cascade do |t|
    t.string   "subcategory", limit: 255, null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "subcategories", ["subcategory"], name: "index_subcategories_on_subcategory", unique: true, using: :btree

  add_foreign_key "items", "categories"
  add_foreign_key "items", "subcategories"
end
