class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :name, :null => false
      t.string :Artist, :null => false
      t.string :Album, :null => false
      t.integer :year, :null => false
      t.text :link, :null => false
      t.integer :Noofdown, :null => false, :default => 0
      t.integer :NoofViews, :null => false, :default => 0
      t.text :Details, :null => false
      t.text :img, :null => false
      t.references :category, index: true, foreign_key: true
      t.references :subcategory, index: true, foreign_key: true
      
      t.timestamps null: false
    end
    
    
  end
end
