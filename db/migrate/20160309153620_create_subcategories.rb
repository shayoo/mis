class CreateSubcategories < ActiveRecord::Migration
  def change
    create_table :subcategories do |t|
      t.string :subcategory, :null => false

      t.timestamps null: false
    end
    add_index :subcategories, :subcategory, unique: true
  end
end
